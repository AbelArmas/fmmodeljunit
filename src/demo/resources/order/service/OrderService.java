package demo.resources.order.service;

import java.util.LinkedList;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import demo.hypermedia.utils.ExtendedLink;
import demo.resources.coffee.CoffeeResource;
import demo.resources.coffee.Type;
import demo.resources.order.CoffeeOrderResource;

public class OrderService extends CoffeeOrderResource {
	RestTemplate restTemplate = new RestTemplate();
	
	String host = "good-coffee-shop.herokuapp.com";
//	String host = "bad-coffee-shop.herokuapp.com";
	String port = "80";
	
	public OrderService() {
		this.start();
	}

	public void start() {
		CoffeeOrderResource order = new CoffeeOrderResource();
		
		ResponseEntity<CoffeeOrderResource> result = restTemplate.postForEntity("http://" + host + ":" + port + "/rest/orders", order, CoffeeOrderResource.class);
		order = result.getBody();
		
		setValues(order);
	}
	
	private void setValues(CoffeeOrderResource order) {
		this.remove_links();
		this.removeLinks();
		
		this.coffees = new LinkedList<CoffeeResource>(order.getCoffees());
		this.status = order.getStatus();
		this.total = order.getTotal();
		
		this.add(order.getLinks());
		this.add(order.get_links());
	}

	public void cancel(){
		ExtendedLink link = this.get_link("cancelOrder");
		String url = link.getHref();
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
	}

	public void addCoffee() {
		ExtendedLink link = this.get_link("addCoffeeToOrder");
		String url = link.getHref();
		
		CoffeeResource coffee = new CoffeeResource();
		coffee.setClientsName("Any name");
		coffee.setType(Type.Espresso);
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<CoffeeResource> httpEntity = new HttpEntity<CoffeeResource>(coffee, requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
		
	}

	public void payOrder() {
		ExtendedLink link = this.get_link("payOrder");
		String url = link.getHref();
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.POST, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
	}

	public void addSugarToCoffee(CoffeeResource coffee) {
		ExtendedLink link = coffee.get_link("addSugarToCoffee");
		String url = link.getHref();
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
	}

	public void finishOrder() {
		ExtendedLink link = this.get_link("finishOrder");
		String url = link.getHref();
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.POST, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
	}

	public void getReceipt() {
		ExtendedLink link = this.get_link("getReceipt");
		String url = link.getHref();
		
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<CoffeeResource> httpEntity = new HttpEntity<CoffeeResource>(requestHeaders);
		ResponseEntity<CoffeeOrderResource> result = null;
		
		result = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, CoffeeOrderResource.class);
		
		setValues(result.getBody());
	}

	public CoffeeResource findCoffee(Link link) {
		for(CoffeeResource coffee : this.coffees)
			if(coffee.getLink("self").getHref().equals(link.getHref()))
				return coffee;
		
		return null;
	}
}
