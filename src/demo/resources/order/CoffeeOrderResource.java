package demo.resources.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import demo.hypermedia.utils.ResourceSupport;
import demo.resources.coffee.CoffeeResource;
import demo.resources.order.CoffeeOrderStatus.Status;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class CoffeeOrderResource extends ResourceSupport{
	protected List<CoffeeResource> coffees;
	protected Status status;
	protected Float total;

}
