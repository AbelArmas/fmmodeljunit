package demo.resources.order;

public class CoffeeOrderStatus{
	public static enum Status {
		PAYMENT_EXPECTED, 
		TAKING_ORDER, 
		PREPARING, 
		CANCELLED, 
		READY, 
		COMPLETED
	}
}