package demo.resources.coffee;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import demo.hypermedia.utils.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class CoffeeResource extends ResourceSupport{
	Float price;
	Type type;
	String clientsName;
	Long sugarCubes;
}