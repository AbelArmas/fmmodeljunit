package demo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import demo.resources.coffee.CoffeeResource;
import demo.resources.order.CoffeeOrderStatus.Status;
import demo.resources.order.service.OrderService;
import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;
import nz.ac.waikato.modeljunit.GreedyTester;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.coverage.CoverageMetric;
import nz.ac.waikato.modeljunit.coverage.TransitionCoverage;

public class EmptyOrderEFSM implements FsmModel {
	OrderService order = new OrderService();

	@Override
	public Status getState() {
		return null;
	}

	@Override
	public void reset(boolean testing) {
	}

	public boolean cancelOrderGuard() {
		return false;}
	public @Action void cancelOrder() {
		
	}

	public boolean addCoffeeToOrderGuard() {
		return false;}
	public @Action void addCoffeeToOrder() {
	}

	public boolean payOrderGuard() {
		return false;}
	public @Action void payOrder() {
	}

	public boolean addSugarToCoffeeGuard() {
		return false;}
	public @Action void addSugarToCoffee() {
	}

	public boolean finishOrderGuard() {
		return false;}
	public @Action void finishOrder() {
	}

	public boolean getReceiptGuard() {
		return false;}
	public @Action void getReceipt() {
	}

	public static void main(String args[]) {

		Tester tester = new GreedyTester(new EmptyOrderEFSM());

		// build the complete FSM graph for our model, just to ensure
		// that we get accurate model coverage metrics.
		tester.buildGraph();

		// set up our favourite coverage metric
		CoverageMetric trCoverage = new TransitionCoverage();
		tester.addListener(trCoverage);

		// ask to print the generated tests
		tester.addListener("verbose");

		// generate a small test suite of 20 steps (covers 4/5 transitions)
		tester.generate(50);

		tester.getModel().printMessage(
				trCoverage.getName() + " was " + trCoverage.toString());
	}
}
