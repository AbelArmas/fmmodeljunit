package demo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import demo.resources.coffee.CoffeeResource;
import demo.resources.order.CoffeeOrderStatus.Status;
import demo.resources.order.service.OrderService;
import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;
import nz.ac.waikato.modeljunit.GreedyTester;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.coverage.CoverageMetric;
import nz.ac.waikato.modeljunit.coverage.TransitionCoverage;

public class OrderEFSM implements FsmModel {
	OrderService order = new OrderService();

	@Override
	public Status getState() {
		return order.getStatus();
	}

	@Override
	public void reset(boolean testing) {
		order.start();

		assertThat(order.getLink("self"), is(notNullValue()));
		assertThat(order.get_link("addCoffeeToOrder"), is(notNullValue()));
		assertThat(order.get_link("cancelOrder"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(3));
	}

	public boolean cancelOrderGuard() {return getState().equals(Status.PAYMENT_EXPECTED);}
	public @Action void cancelOrder() {
		order.cancel();

		assertThat(order.getStatus(), equalTo(Status.CANCELLED));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(1));
	}

	public boolean addCoffeeToOrderGuard() {return getState().equals(Status.PAYMENT_EXPECTED) || getState().equals(Status.TAKING_ORDER);}
	public @Action void addCoffeeToOrder() {
		order.addCoffee();

		assertThat(getState(), equalTo(Status.TAKING_ORDER));
		assertThat(order.getLink("self"), is(notNullValue()));
		assertThat(order.get_link("addCoffeeToOrder"), is(notNullValue()));
		assertThat(order.get_link("payOrder"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(3));
	}

	public boolean payOrderGuard() {
		return getState().equals(Status.TAKING_ORDER);}
	public @Action void payOrder() {
		order.payOrder();
		
		assertThat(getState(), equalTo(Status.PREPARING));
		assertThat(order.getLink("self"), is(notNullValue()));
		
		if(!order.getCoffees().isEmpty()){
			CoffeeResource coffee = order.getCoffees().get(0);
			assertThat(coffee.get_link("addSugarToCoffee"), is(notNullValue()));
		}
		assertThat(order.get_link("finishOrder"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(2));
	}

	public boolean addSugarToCoffeeGuard() {return order.getStatus().equals(Status.PREPARING);}
	public @Action void addSugarToCoffee() {
		CoffeeResource coffee = order.getCoffees().get(0);

		Long sugar = coffee.getSugarCubes();
		order.addSugarToCoffee(coffee);

		assertThat(getState(), equalTo(Status.PREPARING));
		
		assertThat(order.findCoffee(coffee.getLink("self")).getSugarCubes(),
				is(sugar + 1));

		assertThat(order.getLink("self"), is(notNullValue()));
		if(!order.getCoffees().isEmpty()){
			coffee = order.getCoffees().get(0);
			assertThat(coffee.get_link("addSugarToCoffee"), is(notNullValue()));
		}
		assertThat(order.get_link("finishOrder"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(2));
	}

	public boolean finishOrderGuard() {return order.getStatus().equals(Status.PREPARING);}
	public @Action void finishOrder() {
		order.finishOrder();

		assertThat(getState(), equalTo(Status.READY));
		assertThat(order.getLink("self"), is(notNullValue()));
		assertThat(order.get_link("getReceipt"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(2));
	}

	public boolean getReceiptGuard() {return order.getStatus().equals(Status.READY);}
	public @Action void getReceipt() {
		order.getReceipt();

		assertThat(getState(), equalTo(Status.COMPLETED));
		assertThat(order.getLink("self"), is(notNullValue()));
//		assertThat(order.getLinks().size() + order.get_links().size(), is(1));
	}

	public static void main(String args[]) {

		Tester tester = new GreedyTester(new OrderEFSM());

		// build the complete FSM graph for our model, just to ensure
		// that we get accurate model coverage metrics.
		tester.buildGraph();

		// set up our favourite coverage metric
		CoverageMetric trCoverage = new TransitionCoverage();
		tester.addListener(trCoverage);

		// ask to print the generated tests
		tester.addListener("verbose");

		// generate a small test suite of 20 steps (covers 4/5 transitions)
		tester.generate(50);

		tester.getModel().printMessage(
				trCoverage.getName() + " was " + trCoverage.toString());
	}
}
